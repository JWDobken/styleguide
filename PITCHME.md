---?color=#151A2E
@title[title page]

@snap[west text-25 text-bold text-white]
döb<br>*Style Guide*
@snapend

---
A styleguide is a set of standards, principles and rules every developer or designer should follow in order to improve the digital presence of the product.

---

Color variables
```
$brand-primary: $orange; 
$orange: #ed5f43;(the color of the logo and main call to action, the most important buttons)

$btn-primary-bg: $brand-primary; (the main buttons)
$btn-secondary-bg: #a2b2bd; (the color of the second call to action, default buttons)
$btn-secondary-bg-hover: #5b6b79;

$headings-color: #2b363f; (titles, sub-titles)
$body-color: #5b6b79 (paragraphs, ul, ol, li, labels)
$light-bg: #edf1f3;
$dark-bg: #5b6b79;
$input-border-color: #c3ced6;
$input-border-color-focus: #90a3b0;
```
---?color=linear-gradient(to right, #c02425, #f0cb35)
@title[döb]

@snap[west text-25 text-bold text-white]
GitPitch<br>*The Template*
@snapend

@snap[south-west byline text-white text-06]
styleguide.
@snapend

---
@title[Slide Markdown]

### Each slide in this presentation is provided as a *template*.

<br><br>

@snap[south span-100 text-purple text-05]
Reuse the *markdown snippet* for any slide in this template within your own @css[text-gold text-bold](PITCHME.md) files.
@snapend

---
@title[Tip! Fullscreen]

![TIP](template/img/tip.png)
<br>
For the best viewing experience, press F for fullscreen.
@css[template-note](We recommend using the *SPACE* key to navigate between slides.)

---?include=template/md/split-screen/PITCHME.md

---?include=template/md/sidebar/PITCHME.md

---?include=template/md/list-content/PITCHME.md

---?include=template/md/boxed-text/PITCHME.md

---?include=template/md/image/PITCHME.md

---?include=template/md/sidebox/PITCHME.md

---?include=template/md/code-presenting/PITCHME.md

---?include=template/md/header-footer/PITCHME.md

---?include=template/md/quotation/PITCHME.md

---?include=template/md/announcement/PITCHME.md

---?include=template/md/about/PITCHME.md

---?include=template/md/wrap-up/PITCHME.md

---?image=template/img/presenter.jpg
@title[The Template Docs]

@snap[north-west sign-off]
### **Now it's @color[#e58537](your) turn.**
<br>
#### Quickstart your next slide deck<br>with @size[1.4em](The GitPitch Template).
@snapend

@snap[south docslink text-gold span-100]
For supporting documentation see the [The Template Docs](https://gitpitch.com/docs/the-template)
@snapend
